# coreos-save-testing

This project builds a VM using Vagrant and Virtualbox.

The VM comes with a basic set of tools, coreos and ignition, python and pip, docker and docker-compose.

A set of SSH pubkeys are pushed into to VM for administration purposes with ansible for instance.

## Install

At first you must install vagrant with you favorite package manager. Adding the disksize plugin may be
necessary because your VM could exceed the default disk size, 16GB.

```
sudo apt install vagrant virtualbox
vagrant plugin install vagrant-disksize
```

If you get an error *conflicting dependencies fog-core*, you can install the latest version of vagrant package.

```
wget -c https://releases.hashicorp.com/vagrant/2.0.3/vagrant_2.0.3_x86_64.deb
sudo dpkg -i vagrant_2.0.3_x86_64.deb
```

Then clone the repo, build it.

```
git clone https://gitlab.com/fredloze/coreos-save-testing.git
cd coreos-save-testing
vagrant up
```

Congratulations. Check it.
Check that grafana is online ```http://127.0.0.1:8080/login```
and open a console. 

```vagrant ssh```

```console
Last login: Mon May 20 09:59:15 UTC 2019 from 10.0.2.2 on pts/0
Container Linux by CoreOS stable (2079.4.0)
core@core-01 ~ $ docker version
...
core@core-01 ~ $ docker-compose --version
...
core@core-01 ~ $ /opt/python/bin/python --version
...
core@core-01 ~ $ /opt/python/bin/pip --version
...
core@core-01 ~ $ ls -lR .ssh/
...
core@core-01 ~ $ docker ps
...
```


Destroy the VM, edit the pubkeys file, and build again.

```console
core@core-01 ~ $ exit
```
```
vagrant destroy
vi provision/group_vars/user_keys.yml
vagrant up
```

## next steps

We have to deploys containers, and backup them.

What software will we choose:

- ansible -m docker-compose
- ansible -m containers (?)
- duplicity ?
- duply ?
- borg backup ?

experiments are there : [containers-testing on gitlab](https://gitlab.com/fredloze/containers-testing)


## Credits

- [coreos-vagrant at github](https://github.com/coreos/coreos-vagrant)
- [instrumentisto at gtithub](https://github.com/instrumentisto/ansible-coreos-bootstrap)
- [kellerfuchs at github](https://github.com/hashbang/ansible-authorized_keys.d)

