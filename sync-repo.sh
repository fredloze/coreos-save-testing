#!/bin/bash
#
# script sync-repo.sh
#
# Two repo : * coreos-save-testing to make local vagrant boxes, easy destroy
#            * containers-testing  to make cloud VM, in AWS and OVH
# The two repo share ansible playbook directives.


(
  cd $HOME/containers-testing
  git commit -m ' automatic synchro begin '
  rsync --cvs-exclude -va \
	../coreos-save-testing/provision/ ./ \
        --exclude-from=- <<EOF
roles
main.*
inventory
requirements.yml
group_vars
README.*
EOF
  git commit -m ' automatic synchro end '
)

